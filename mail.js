require('dotenv').config();

var nodemailer = require('nodemailer');

module.exports = (mailOptions) => {
   let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          // type: "OAuth2",
          user: process.env.EMAIL,
          pass: process.env.PASSWORD
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log(info);
        }
    }); 
} 
