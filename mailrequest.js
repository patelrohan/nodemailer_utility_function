const mailFunc = require('./mail');
const _ = require('lodash')

var compiled = _.template(`<p><b style = "color: blue;font-size: 50px;">Hello ,</b></p>\n<p><%= user %>!</p>\n\n<div><img height="30%" width="30%" src = "cid:image1"></div>\n<b>Thanks and Regards</b>`);
var c = compiled({ 'user': `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s` });

mailFunc({
    from: 'rohanpatel@credenceanalytics.com',
    to: 'rohanpatel@credenceanalytics.com', 
    subject: '(sent using Nodemailer)',
    cc:'rohanpatel@credenceanalytics.com',
    attachments: [
                {
                    "filename": "wp3205255.jpg",
                    "path": "D:/Rohan/NodejsSTREAMS&Nodemailer/wp3205255.jpg",
                    "cid": "image1"
                }
            ],
    html:  `${c}`

})
